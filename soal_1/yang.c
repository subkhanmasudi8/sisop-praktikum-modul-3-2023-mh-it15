#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROWS 5
#define COLS 5

struct threaddata {
    int row;
    int col;
};

unsigned long long faktorial(int n) {
    if (n <= 1) {
        return 1;
    } else {
        unsigned long long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}

void *computefactorial(void *arg) {
    struct threaddata *data = (struct threaddata *)arg;
    int row = data->row;
    int col = data->col;

    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(int) * ROWS * COLS, 0666);
    int *shared_memory = (int *)shmat(shmid, (void *)0, 0);
    unsigned long long result = faktorial(shared_memory[row * COLS + col]);
    shared_memory[row * COLS + col] = result;
    shmdt(shared_memory);

    pthread_exit(NULL);
}

int main() {
    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(int) * ROWS * COLS, 0666);
    int *shared_memory = (int *)shmat(shmid, (void *)0, 0);

    printf("Matriks Hasil Transpose :\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d", shared_memory[i * COLS + j]);
            if (j < COLS - 1) {
                printf("\t");
            }
        }
        printf("\n");
    }

    pthread_t threads[ROWS][COLS];
    struct threaddata data[ROWS][COLS];
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            data[i][j].row = i;
            data[i][j].col = j;
            pthread_create(&threads[i][j], NULL, computefactorial, &data[i][j]);
        }
    }

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }

    printf("Matriks Setelah Perhitungan Faktorial:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%llu", (unsigned long long)shared_memory[i * COLS + j]);
            if (j < COLS - 1) {
                printf("\t");
            }
        }
        printf("\n");
    }

    shmdt(shared_memory);

    return 0;
}