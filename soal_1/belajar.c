#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

void printmatrix(int matrix[5][5], int rows, int columns, const char *matrixname) {
    printf("%s:\n", matrixname);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main() {
    int matriks1[5][2];
    int matriks2[2][5];
    int hasil[5][5];

    srand(time(NULL));

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 4 + 1;
        }
    }

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 5 + 1;
        }
    }

    printmatrix(matriks1, 5, 2, "Matriks 1");
    printmatrix(matriks2, 2, 5, "Matriks 2");

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = 0;
        }
    }

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            for (int k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
            hasil[i][j] -= 1;
        }
    }

    printf("Matriks Hasil Dikurangi 1):\n");
    printmatrix(hasil, 5, 5, "Matriks Hasil");

    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(hasil), 0666 | IPC_CREAT); 
    int *shared_memory = (int *)shmat(shmid, (void *)0, 0); 

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            shared_memory[i * 5 + j] = hasil[i][j];
        }
    }

    shmdt(shared_memory);

    return 0;
}