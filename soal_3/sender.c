#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <stdbool.h>
#include <libgen.h>

struct message {
    long mtype;
    char mtext[256];
};

void download(const char* url, const char* outputFolder) {
    char command[256];
    snprintf(command, sizeof(command), "wget --no-check-certificate -q -O %s/file.zip %s", outputFolder, url);
    system(command);

    snprintf(command, sizeof(command), "unzip -q %s/file.zip -d %s", outputFolder, outputFolder);
    system(command);

    snprintf(command, sizeof(command), "rm %s/file.zip", outputFolder);
    system(command);
}

int main() {
    key_t key = ftok("receiver.c", 65);
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    struct message msg;
    msg.mtype = 1;

    printf("Masukkan (CREDS/AUTH: username password/TRANSFER filename/DOWNLOAD): ");
    fgets(msg.mtext, sizeof(msg.mtext), stdin);

    if (strchr(msg.mtext, '\n') != NULL) {
        msg.mtext[strcspn(msg.mtext, "\n")] = '\0';
    }

    if (strstr(msg.mtext, "TRANSFER") != NULL) {
        char filename[256];
        sscanf(msg.mtext, "TRANSFER %s", filename);

        char userfile[512]; 
        snprintf(userfile, sizeof(userfile), "Sender/%s", filename);


        FILE* filesend = fopen(userfile, "r");
        if (filesend == NULL) {
            perror("fopen");
            exit(1);
        }


        struct message filemass;
        filemass.mtype = 2;  
        fread(filemass.mtext, 1, sizeof(filemass.mtext), filesend);

        if (msgsnd(msgid, &filemass, sizeof(filemass.mtext), 0) == -1) {
            perror("msgsnd");
            exit(1);
        }

        printf("File '%s' has sent.\n", filename);

        fclose(filesend);
    } else if (strcmp(msg.mtext, "DOWNLOAD") == 0) {
        // Unduh dan unzip file
        download("https://drive.google.com/uc?id=1CrERpikZwuxgAwDvrhRnB2VyAYtT2SIf", ".");
    } else {
        if (msgsnd(msgid, &msg, sizeof(msg.mtext), 0) == -1) {
            perror("msgsnd");
            exit(1);
        }

    }

    return 0;
}
