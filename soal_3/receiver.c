#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <libgen.h> 

struct message {
    long mtype;
    char mtext[256];
};

void decodebase64(const char* input, char* output) {
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    int decodedlen = BIO_read(bio, output, strlen(input));
    output[decodedlen] = '\0'; 
    BIO_free_all(bio);
}

int main() {
    key_t key = ftok("receiver.c", 65);
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    struct message msg;

    if (msgrcv(msgid, &msg, sizeof(msg.mtext), 1, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }

    FILE* file = fopen("users/users.txt", "r");
    if (file == NULL) {
        perror("fopen");
        exit(1);
    }

    if (strcmp(msg.mtext, "CREDS") == 0) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char username[256];
            char password[256];
            sscanf(line, "%[^:]:%s", username, password);

            char decodedpasword[256];
            decodebase64(password, decodedpasword);

            printf("Username: %s, Password: %s\n", username, decodedpasword);
        }
    } else if (strstr(msg.mtext, "AUTH:") != NULL) {
        char username[256];
        char password[256];
        sscanf(msg.mtext, "AUTH: %s %s", username, password);

        char line[256];
        int authenticated = 0;
        while (fgets(line, sizeof(line), file)) {
            char storedusername[256];
            char storedpasword[256];
            sscanf(line, "%[^:]:%s", storedusername, storedpasword);

            char decodedpasword[256];
            decodebase64(storedpasword, decodedpasword);

            if (strcmp(username, storedusername) == 0 && strcmp(password, decodedpasword) == 0) {
                authenticated = 1;
                break;
            }
        }

        if (authenticated) {
            printf("Authentication successful.\n");
        } else {
            printf("Authentication failed.\n");
            exit(1); 
        }
    } else if (strstr(msg.mtext, "TRANSFER") != NULL) {
        char filename[256];
        sscanf(msg.mtext, "TRANSFER %s", filename);

        if (access("Receiver", F_OK) == -1) {
            mkdir("Receiver", 0777);
        }

        char receiverfile[512]; 
        snprintf(receiverfile, sizeof(receiverfile), "Receiver/%s", basename(filename));

        FILE* fileexist = fopen(receiverfile, "r");
        if (fileexist) {
            printf("File '%s' already exists.\n", filename);
            fclose(fileexist);
        } else {
            FILE* receivedFile = fopen(receiverfile, "w");
            fwrite(msg.mtext, 1, strlen(msg.mtext), receivedFile);
            fclose(receivedFile);
            printf("File '%s' transferred to Receiver.\n", filename);
        }
    }

    fclose(file);

    return 0;
}
