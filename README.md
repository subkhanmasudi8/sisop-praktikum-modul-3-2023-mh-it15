# sisop-praktikum-modul-3-2023-MH-IT15
Laporan Resmi pengerjaan soal shift modul 3 Praktikum Sistem Operasi 2023 Kelompok IT15

## Anggota Kelompok
1. Ilhan Ahmad Syafa (5027221040)
2. Subkhan Masudi (5027221044)
3. Gilang Raya Kurniawan (5027221045)

# SOAL 1
Epul memiliki seorang adik SMA bernama Azka. Azka bersekolah di Thursina Malang. Di sekolah, Azka diperkenalkan oleh adanya kata matriks. Karena Azka baru pertama kali mendengar kata matriks, Azka meminta tolong kepada kakaknya Epul untuk belajar matriks.  Karena Epul adalah seorang kakak yang baik, Epul memiliki ide untuk membuat program yang bisa membantu adik tercintanya, tetapi dia masih bingung untuk membuatnya. Karena Epul adalah bagian dari IT05 dan praktikan sisop, sebagai warga IT05 yang baik, bantu Epul mengerjakan programnya dengan ketentuan sebagai berikut :

## Problem Soal 1


1. Membuat program C dengan nama belajar.c, yang berisi program untuk melakukan perkalian matriks. Agar ukuran matriks bervariasi, maka ukuran matriks pertama adalah [nomor_kelompok]×2 dan matriks kedua 2×[nomor_kelompok]. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-4 (inklusif), dan rentang pada matriks kedua adalah 1-5 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar. 

2. Karena Epul merasa ada yang kurang, Epul meminta agar hasil dari perkalian tersebut dikurang 1 di setiap matriks.

3. Karena Epul baru belajar modul 3, Epul ngide ingin meminta agar menerapkan konsep shared memory. Buatlah program C kedua dengan nama yang.c. Program ini akan mengambil variabel hasil pengurangan dari perkalian matriks dari program belajar.c (program sebelumnya). Hasil dari pengurangan perkalian matriks tersebut dilakukan transpose matriks dan diperlihatkan hasilnya. 
(Catatan: wajib menerapkan konsep shared memory)

4. Setelah ditampilkan, Epul pengen adiknya belajar lebih, sehingga untuk setiap angka dari transpose matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 

**matriks **
   
    1	2  	3

    2	2	4

**maka:**

    1	4	6

    4	4	24

(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)

5. Epul penasaran mengapa dibuat thread dan multithreading pada program sebelumnya, dengan demikian dibuatlah program C ketiga dengan nama rajin.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada yang.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

**Catatan:**
Key memory diambil berdasarkan program dari belajar.c sehingga program belajar.c tidak perlu menghasilkan key memory (value). Dengan demikian, pada program yang.c dan rajin.c hanya perlu mengambil key memory dari belajar.c
Untuk kelompok 1 - 9, 11 - 19, 21 menggunakan  digit akhir saja.

	Contoh : 19 -> 9
Untuk kelompok 10 dan 20 menambahkan digit awal dan akhir.

	Contoh : 20 - > 2

## Solution soal 1
https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-3-2023-mh-it15/-/tree/main/soal_1?ref_type=heads

Solution Soal 2A
Pertama tama buat program dengan nama **belajar.c**

**Lalu masukan code sebagai berikut **

```
void printmatrix(int matrix[5][5], int rows, int columns, const char *matrixname) {
    printf("%s:\n", matrixname);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main() {
    int matriks1[5][2];
    int matriks2[2][5];
    int hasil[5][5];

    srand(time(NULL));

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 4 + 1;
        }
    }

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 5 + 1;
        }
    }

    printmatrix(matriks1, 5, 2, "Matriks 1");
    printmatrix(matriks2, 2, 5, "Matriks 2");

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = 0;
        }
    }
```

Pada baris code ini program akan mendefinisikan fungsi **printmatrix** yang digunakan untuk mencetak matriks ke layar. Fungsi ini menerima matriks, jumlah baris, jumlah kolom, dan nama matriks sebagai argumen.
```
void printmatrix(int matrix[5][5], int rows, int columns, const char *matrixname) {
    printf("%s:\n", matrixname);
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
```
Pada fungsi dibawah, program membuat tiga matriks **matriks1, matriks2, dan hasil** sebagai variabel lokal. Matriks matriks1 memiliki dimensi 5x2, matriks2 memiliki dimensi 2x5. digunakan fungsi **srand** untuk mengeksekusi generator bilangan acak, sehingga hasilnya akan bervariasi setiap kali program dijalankan.
```
int main() {
    int matriks1[5][2];
    int matriks2[2][5];
    int hasil[5][5];

    srand(time(NULL));

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 2; j++) {
            matriks1[i][j] = rand() % 4 + 1;
        }
    }

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 5; j++) {
            matriks2[i][j] = rand() % 5 + 1;
        }
    }

    printmatrix(matriks1, 5, 2, "Matriks 1");
    printmatrix(matriks2, 2, 5, "Matriks 2");

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            hasil[i][j] = 0;
        }
    }
```

## Solution Soal 1B

```
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            for (int k = 0; k < 2; k++) {
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
            hasil[i][j] -= 1;
        }
    }

    printf("Matriks Hasil Dikurangi 1):\n");
    printmatrix(hasil, 5, 5, "Matriks Hasil");
```
pada Kode di atas, terjadi perhitungan perkalian matriks antara matriks1 dan matriks2 yang telah dilakukan sebelumnya. Hasil perkalian kemudian disimpan di dalam matriks hasil. Setelah selesai menghitung hasil perkalian matriks, program akan mengurangi 1 dari setiap perhitungan matriks hasil **misal (48 menjadi 47)**. lalu program akan mencetak matriks hasil ke layar menggunakan fungsi **printmatrix**

## Solution Soal 1C dan D
Pertama tama buat dahulu program dengan name **yang.c**

lalu masukkan code
```
struct threaddata {
    int row;
    int col;
};

unsigned long long faktorial(int n) {
    if (n <= 1) {
        return 1;
    } else {
        unsigned long long result = 1;
        for (int i = 2; i <= n; i++) {
            result *= i;
        }
        return result;
    }
}

void *computefactorial(void *arg) {
    struct threaddata *data = (struct threaddata *)arg;
    int row = data->row;
    int col = data->col;

    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(int) * ROWS * COLS, 0666);
    int *shared_memory = (int *)shmat(shmid, (void *)0, 0);
    unsigned long long result = faktorial(shared_memory[row * COLS + col]);
    shared_memory[row * COLS + col] = result;
    shmdt(shared_memory);

    pthread_exit(NULL);
}

int main() {
    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(int) * ROWS * COLS, 0666);
    int *shared_memory = (int *)shmat(shmid, (void *)0, 0);

    printf("Matriks Hasil Transpose :\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d", shared_memory[i * COLS + j]);
            if (j < COLS - 1) {
                printf("\t");
            }
        }
        printf("\n");
    }

    pthread_t threads[ROWS][COLS];
    struct threaddata data[ROWS][COLS];
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            data[i][j].row = i;
            data[i][j].col = j;
            pthread_create(&threads[i][j], NULL, computefactorial, &data[i][j]);
        }
    }

    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            pthread_join(threads[i][j], NULL);
        }
    }

    printf("Matriks Setelah Perhitungan Faktorial:\n");
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%llu", (unsigned long long)shared_memory[i * COLS + j]);
            if (j < COLS - 1) {
                printf("\t");
            }
        }
        printf("\n");
    }

```
Program ini merupakan lanjutan dari perhitungan yang dilakukan pada matriks hasil yang telah dihasilkan oleh **belajar.c**. kode ini menggunakan konsep shared memory dan thread untuk menghitung transponen lalu faktorial dari perhitungan yang sudah dilakukan oleh program sebelumnya.
```
void *computefactorial(void *arg) {
    struct threaddata *data = (struct threaddata *)arg;
    int row = data->row;
    int col = data->col;

    key_t key = ftok("shared_memory_key", 65);
    int shmid = shmget(key, sizeof(int) * ROWS * COLS, 0666);
    int *shared_memory = (int *)shmat(shmid, (void *)0, 0);
    unsigned long long result = faktorial(shared_memory[row * COLS + col]);
    shared_memory[row * COLS + col] = result;
    shmdt(shared_memory);

    pthread_exit(NULL);
}
```
Program dimulai dengan mengambil shared memory dari program **Belajar.c**. lalu mencetak matriks hasil transposenya ke layar. Kemudian, program membuat sebuah arraybdari thread untuk menghitung faktorial dan struktur **threaddata** yang berisi informasi baris dan kolom matriks yang akan dihitung faktorialnya.

Setelah semua thread selesai dieksekusi dengan bantuan **pthread_join**, lalu program mencetak matriks hasil transpone dan faktorial ke layar terminal

##Penggerjaan Soal 4
1. pertama baut file belajar.c di direktory

2. lalu jalankan belajar.c yang dimana hasilnya akan seperti ini 

<img src="https://i.ibb.co/QJCqt0d/1a.png" alt="1a" border="0">
dimana matriks 1, matriks 2 dan hasil perkalian matriks 1 dan 2 (dikurangi 1) akan ditamilkan dilayar


3. lalu setelah itu eksekusi kode yang.c untuk menampilan matriks transponen dan faktorialnya

<img src="https://i.ibb.co/Y3j3cw9/1b.png" alt="1b" border="0">

# SOAL 2

Messi the only GOAT sedang gabut karena Inter Miami Gagal masuk Playoff. Messi menghabiskan waktu dengan mendengar lagu. Karena gabut yang tergabut gabut, Messi penasaran berapa banyak jumlah kata tertentu yang muncul pada lagu tersebut. Messi mencoba ngoding untuk mencari jawaban dari kegabutannya. Pertama-tama dia mengumpulkan beberapa lirik lagu favorit yang disimpan di file. Beberapa saat setelah mencoba ternyata ngoding tidak semudah itu, Messipun meminta tolong kepada Ronaldo, tetapi karena Ronaldo sibuk main di liga oli, Ronaldopun meminta tolong kepadamu untuk dibuatkan kode dari program Messi dengan ketentuan sebagai berikut : 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

## Problem Soal 2
A. Messi ingin nama programnya **8ballondors.c**. Pada parent process, program akan membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama **thebeatles.txt**

B. Pada child process, lakukan perhitungan jumlah frekuensi kemunculan sebuah kata dari output file.

C. Karena gabutnya, Messi ingin program dapat mencari jumlah frekuensi kemunculan sebuah huruf dari file. Maka, pada child process lakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari output file. 

D. Karena terdapat dua perhitungan, maka pada program buatlah argumen untuk menjalankan program : 
    i.  **Kata**	: ./8ballondors -kata
    ii. **Huruf**	: ./8ballondors -huruf

E. Hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi kemunculan sebuah huruf dikirim ke parent process.

F. Messi ingin agar setiap kata atau huruf dicatat dalam sebuah log yang diberi nama **frekuensi.log**. Pada parent process, lakukan pembuatan file log berdasarkan data yang dikirim dari child process.

    i.   Format : [date] [type] [message]

    ii.  Type   : KATA, HURUF

    iii. Ex     :
            1. [24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'

            2. [24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'

**Catatan:**

    - Perhitungan jumlah frekuensi kemunculan kata atau huruf  menggunakan Case-Sensitive Search.

## Solution Soal 2
[Source Code Soal 1](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-3-2023-mh-it15/-/tree/main/soal_2)

### Solution Soal 2 A
siapkan directory untuk mengerjakan soal dengan cara :

```
cd sisopmodul3

```

kalian bisa untuk menyiapkan file yang harus didownload dengan cara

```
wget -O lirik_lagu_favorit.txt "https://drive.google.com/uc?export=download&id=16FQ11ynf1hSpREPU1jlmzj6xvNl8oosW"

```
maka file **lirik_lagu_favorit** akan terdownload dan akan berada di direktory yang sudah dibuat diawal.

setelah itu kita bisa membuat 8ballondors.c dengan cara :

```
nano 8ballondors.c
```
kita masukkan code untuk bisa membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama **thebeatles.txt**

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#define MAX_WORD_LENGTH 999

void remove_non_letters(char *str) {
    int i = 0, j = 0;
    while (str[i]) {
        if (isalpha(str[i]) || str[i] == ' ' || str[i] == '\n') {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

void count_letter_frequency(FILE *file) {
    int count[26] = {0};
    char c;

    while ((c = fgetc(file)) != EOF) {
        if (isalpha(c)) {
            c = tolower(c);
            count[c - 'a']++;
        }
    }

    for (int i = 0; i < 26; i++) {
        printf("Frekuensi kemunculan huruf '%c': %d\n", i + 'a', count[i]);
    }
}

int main() {
    int fd[2];
    pid_t pid;

    pipe(fd);

    pid = fork();

    if (pid == 0) {
        close(fd[1]);

        FILE *file = fopen("thebeatles.txt", "r");
        count_letter_frequency(file);
        fclose(file);

        close(fd[0]);
    } else {
        close(fd[0]);

        FILE *input_file = fopen("lirik_lagu_favorit.txt", "r");
        FILE *output_file = fopen("thebeatles.txt", "w");

        char str[MAX_WORD_LENGTH];

        while (fgets(str, MAX_WORD_LENGTH, input_file) != NULL) {
            remove_non_letters(str);
            fputs(str, output_file);
        }

        fclose(input_file);
        fclose(output_file);

        close(fd[1]);
    }

    return 0;
}

```
code yang kita masukkan merupakan program yang bisa membaca file dan menghapus karakter-karakter yang bukan huruf pada file dan membuat output file dari hasil proses itu dengan nama **thebeatles.txt**

kita bisa menjalankan program dengan cara :

```
gcc 8ballondors.c -o 8ballondors
```
dan untuk running :

```
./8ballondors
```
maka akan muncul seperti ini :

<a href="https://imgbb.com/"><img src="https://i.ibb.co/9GfWrK1/image.png" alt="image" border="0"></a>

dan saat kita cat maka akan muncul seperti ini :

<a href="https://ibb.co/pvw72h1"><img src="https://i.ibb.co/Fb3r6Hh/image.png" alt="image" border="0"></a>

### Solution Soal 2 B

pada soal 2 B disuruh untuk menghitung jumlah frekuensi kemunculan sebuah kata dari output file. kita bisa melakukan perhitungan program seperti ini :

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#define MAX_WORD_LENGTH 999

void remove_non_letters(char *str) {
    int i = 0, j = 0;
    while (str[i]) {
        if (isalpha(str[i]) || str[i] == ' ' || str[i] == '\n') {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

void count_letter_frequency(FILE *file) {
    int count[52] = {0};
    char c;

    while ((c = fgetc(file)) != EOF) {
        if (isalpha(c)) {
            if (isupper(c)) {
                count[c - 'A']++;
            } else {
                count[c - 'a' + 26]++;
            }
        }
    }

    for (int i = 0; i < 26; i++) {
        printf("Frekuensi kemunculan huruf '%c': %d\n", i + 'A', count[i]);
        printf("Frekuensi kemunculan huruf '%c': %d\n", i + 'a', count[i + 26]);
    }
}

int main() {
    int fd[2];
    pid_t pid;

    pipe(fd);

    pid = fork();

    if (pid == 0) {
        close(fd[1]);

        FILE *file = fopen("thebeatles.txt", "r");
        count_letter_frequency(file);
        fclose(file);

        close(fd[0]);
    } else {
        close(fd[0]);

        FILE *input_file = fopen("lirik_lagu_favorit.txt", "r");
        FILE *output_file = fopen("thebeatles.txt", "w");

        char str[MAX_WORD_LENGTH];

        while (fgets(str, MAX_WORD_LENGTH, input_file) != NULL) {
            remove_non_letters(str);
            fputs(str, output_file);
        }

        fclose(input_file);
        fclose(output_file);

        close(fd[1]);
    }

    return 0;
}

```
kita bisa menambahkan pada bagian count_letter_frequency agar program bisa menghitung frekuensi kata yang muncul ketika di check, dan akan muncul output seperti ini :

<a href="https://imgbb.com/"><img src="https://i.ibb.co/FnPHppy/image.png" alt="image" border="0"></a>


### Solution Soal 2 C

disini kita disuruh untuk program dapat mencari jumlah frekuensi kemunculan sebuah huruf dari file. Maka, pada child process lakukan perhitungan jumlah frekuensi kemunculan sebuah huruf dari output file. kita bisa menggunakan program seperti ini

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#define MAX_WORD_LENGTH 999

void remove_non_letters(char *str) {
    int i = 0, j = 0;
    while (str[i]) {
        if (isalpha(str[i]) || str[i] == ' ' || str[i] == '\n') {
            str[j++] = str[i];
        }
        i++;
    }
    str[j] = '\0';
}

void count_letter_frequency(FILE *file) {
    int count[52] = {0};
    char c;

    while ((c = fgetc(file)) != EOF) {
        if (isalpha(c)) {
            if (isupper(c)) {
                count[c - 'A']++;
            } else {
                count[c - 'a' + 26]++;
            }
        }
    }

    for (int i = 0; i < 26; i++) {
        printf("Frekuensi kemunculan huruf '%c': %d\n", i + 'A', count[i]);
        printf("Frekuensi kemunculan huruf '%c': %d\n", i + 'a', count[i + 26]);
    }
}

int main() {
    int fd[2];
    pid_t pid;

    pipe(fd);

    pid = fork();

    if (pid == 0) {
        close(fd[1]);

        FILE *file = fopen("thebeatles.txt", "r");
        count_letter_frequency(file);
        fclose(file);

        close(fd[0]);
    } else {
        close(fd[0]);

        FILE *input_file = fopen("lirik_lagu_favorit.txt", "r");
        FILE *output_file = fopen("thebeatles.txt", "w");

        char str[MAX_WORD_LENGTH];

        while (fgets(str, MAX_WORD_LENGTH, input_file) != NULL) {
            remove_non_letters(str);
            fputs(str, output_file);
        }

        fclose(input_file);
        fclose(output_file);

        close(fd[1]);
    }

    return 0;
}

```
untuk hasil code soal c ini sama dengan b, dikarenakan perhitungan c nya tetap sama hanya saja untuk yg sekarang soal meminta untuk menghitung frekuensi huruf dan akan mengahsilkan output seperti ini : 

<a href="https://ibb.co/5YgMG8g"><img src="https://i.ibb.co/KNp0bqp/image.png" alt="image" border="0"></a>

### Solution Soal 2 D
pada soal 2 D kita diminta untuk buatlah argumen untuk menjalankan program : 
Kata	: ./8ballondors -kata
Huruf	: ./8ballondors -huruf

kita bisa menambahkan bagian argumen pipe pada code yang sebelumnya

```
int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: ./8ballondors -kata|-huruf\n");
        return 1;
    }

    int fd[2];
    pid_t pid;

    pipe(fd);

    pid = fork();

    if (pid == 0) {
        close(fd[1]);

        FILE *file = fopen("thebeatles.txt", "r");
        
        if (strcmp(argv[1], "-kata") == 0) {
            count_word_frequency(file);
        } else if (strcmp(argv[1], "-huruf") == 0) {
            count_letter_frequency(file);
        } else {
            printf("Invalid argument. Use -kata or -huruf.\n");
            return 1;
        }

        fclose(file);

        close(fd[0]);
   } else {
        // code yang lain
    }

    return 0;
}
```
dengan menambahkan code tersebut otomatis saat kita run maka kita harus run dengan cara yg berbeda yaitu dengan :

```
./8ballondors -kata / -huruf 
```
agar program mau berjalan

### Solution Soal 2 E
pada soal 2 E kita disuruh untuk mengirim hasil dari perhitungan  jumlah frekuensi kemunculan sebuah kata dan  jumlah frekuensi ke parent process. kita bisa menggunakan code :

```
// ...

if (pid == 0) {
    // ...
    
    int count;
    if (strcmp(argv[1], "-kata") == 0) {
        count_word_frequency(file, &count);
    } else if (strcmp(argv[1], "-huruf") == 0) {
        count_letter_frequency(file, &count);
    } else {
        printf("Invalid argument. Use -kata or -huruf.\n");
        return 1;
    }

    // Mengirim hasil perhitungan melalui pipa
    write(fd[1], &count, sizeof(count));

    fclose(file);
    close(fd[1]);
    
    exit(0);
} else {
    // ...
}

```
dengan memasukkan code tersebut maka hasil dari perhitungan jumlah frekuensi kemunculan sebuah kata dan jumlah frekuensi ke parent process bisa dijalankan

### Solution Soal 2 F
pada soal 2 F kita disuruh untuk mengirim dan mencatat setiap kata atau huruf dalam sebuah log menuju frekuensi.log dengan beberapa syarat penamaan yaitu 
  i.   Format : [date] [type] [message]

  ii.  Type   : KATA, HURUF
  
  iii. Ex     :
  ```
            1. [24/10/23 01:05:48] [KATA] Kata 'yang' muncul sebanyak 10 kali dalam file 'thebeatles.txt'

            2. [24/10/23 01:04:29] [HURUF] Huruf 'a' muncul sebanyak 396 kali dalam file 'thebeatles.txt'
  
  ```

# kita bisa menambahkan code :

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>


int main(int argc, char *argv[]){

    //banyak yang perlu dideklarasi
    char namafile[] = "lirik.txt";
    char namafile_output[] = "thebeatles.txt";
    char logfile[] = "frekuensi.log";
    char input[1000];
    char chek;
    int count = 0, len;

    //untuk input -kata/-huruf
    int is_kata = strcmp(argv[1], "-kata") == 0;
    int is_huruf = strcmp(argv[1], "-huruf") == 0;

    //waktu buat log
    time_t sekarang;
    time(&sekarang);
    struct tm *local = localtime(&sekarang);
    char timedata[20];
    strftime(timedata, 20, "%d/%m/%y %H:%M:%S", local);

    int fd1[2], fd2[2]; //2 pipa

    //bagian input program
    if(is_huruf){
        printf("masukkan huruf: ");
    }
    else if(is_kata){
        printf("masukkan kata: ");
    }
    scanf("%s", input);

    pid_t pid;

    //memastikan pipa bekerja
    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        fprintf(stderr, "Pipe failed");
        return 1;
    }

    pid = fork();

    if (pid < 0) {
        fprintf(stderr, "Fork failed");
        return 1;
    }
    /*                                        parent process                                    */
    if (pid > 0) {
        //------------------------------------------------------------------------------------- [1]
        //awali dengan ini -1-
        char con[1000];
        close(fd1[0]); //1

        //ini ada yang perlu diwrite
        FILE *file = fopen(namafile, "r");                                  
        FILE *file_output = fopen(namafile_output, "w");                     

        char hurufd;                                                            //sebagai pointer 
        while((hurufd = fgetc(file)) != EOF){                                   //jika file lirik belum EOF periksa terus
            if(isalpha(hurufd) || isspace(hurufd)){                             //jika dia huruf dan spasi
                fputc(hurufd, file_output);                                     //maka isinya bakal nambah
            }
        }
        fclose(file);                                                           //close file
        fclose(file_output);                                                    //close file_output
        write(fd1[1], input, strlen(input)+1);                                  //mengirim input ke child
        close(fd1[1]); // 2

        printf("done ubah liriknya\n");
        //------------------------------------------------------------------------------------- [\1]

        wait(NULL);                                                             //nunggu childe proses

        //------------------------------------------------------------------------------------- [3]
        close(fd2[1]); // 5
        // Hasil akhir

        read(fd2[0], con, 1000);
        FILE *filelog = fopen(logfile, "a");
        if(is_kata){
            fprintf(filelog, "[%s] [KATA] Kata '%s' muncul sebanyak %s kali dalam file '%s'.\n", timedata, input, con, namafile_output);
        } 
        if(is_huruf){
            fprintf(filelog, "[%s] [HURUF] Huruf '%s' muncul sebanyak %s kali dalam file '%s'.\n", timedata, input, con, namafile_output);
        }

        fclose(filelog);

        printf("done frekuensi\n");

        
        close(fd2[0]); // 6
        //------------------------------------------------------------------------------------- [\3]
        printf("done all\n\n");
    }
     /*                                        child process                                      */
    else{
    
        close(fd1[1]); // 2
        //------------------------------------------------------------------------------------- [2]
        //nyambungin dari parent ke child
        char con[1000];
        read(fd1[0], con, 1000);
        printf("done buffer\n");

        close(fd1[0]); // 3
        close(fd2[0]); // 4

        FILE *output_file = fopen(namafile_output, "r");             
        if(is_kata){
            len = strlen(input);

            while((chek = fgetc(output_file)) != EOF){                   //memeriksa input dengan isi file thebeatles.txt
                if(chek == input[0]){
                    int i;
                    for(i = 1; i < len; i++){                           
                        chek = fgetc(output_file);
                        if(chek != input[i]){
                            break;
                        }    
                    }
                    if(i == len){                                        
                        count ++;
                    }
                }
            }
        }
        else if(is_huruf){
            len = strlen(input);

            while((chek = fgetc(output_file)) != EOF){
                if(chek == input[0]){
                    int i;
                    for(i = 1; i < len; i++){
                        chek = fgetc(output_file);
                        if(chek != input[i]){
                            break;
                        }    
                    }
                    if(i == len){
                        count ++;
                    }
                }
            }
        }
        fclose(output_file);
        printf("done ngitung\n");
        if(is_kata){
        printf("kata '%s' muncul sebanyak %d kali dalam file '%s'.\n", input, count, namafile_output);
        }
        if(is_huruf){
        printf("huruf '%s' muncul sebanyak %d kali dalam file '%s'.\n", input, count, namafile_output);
        }
        snprintf(con, sizeof(con), "%d", count);                              
        write(fd2[1], con, strlen(con) + 1); 
        close(fd2[1]); 
        }
    

    return 0;
}


```

code ini bertujuan agar hasil dari process yang sudah di simpan di pipe di kirim dan dicatat ditaruh di file frekuensi.log. dengan catatan Perhitungan jumlah frekuensi kemunculan kata atau huruf  menggunakan Case-Sensitive Search.

jadi untuk run code tersebut adalah dengan cara : 

```
./8ballondors -kata  [kata yang ingin dicari] untuk menghitung frekuensi kata dalam file.
./8ballondors -huruf [huruf yang ingin dicari] untuk menghitung frekuensi huruf dalam file.

```

dan akan muncul output seperti ini :

<a href="https://ibb.co/vmMG4Jq"><img src="https://i.ibb.co/92dLHqg/image.png" alt="image" border="0"></a>

maka apa yg ditugaskan oleh soal dan yg diharapkan oleh messi sudah terlaksana dan berhasil di jalankan.

# SOAL 3
Christopher adalah seorang praktikan sisop, dia mendapat tugas dari pak Nolan untuk membuat komunikasi antar proses dengan menerapkan konsep message queue. Pak Nolan memberikan kredensial list  users yang harus masuk ke dalam program yang akan dibuat. Lebih lanjutnya pak Nolan memberikan instruksi tambahan sebagai berikut : 

## Problem Soal 3


- Bantulah Christopher untuk membuat program tersebut, dengan menerapkan konsep message queue(wajib) maka buatlah 2  program, sender.c sebagai pengirim dan receiver.c sebagai penerima. Dalam hal ini, sender hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh receiver.
Struktur foldernya akan menjadi seperti berikut

    └── soal3
	├── users
	├── receiver.c
	└── sender.c

- Ternyata list kredensial yang diberikan Pak Nolan semua passwordnya terenkripsi dengan menggunakan base64. Untuk mengetahui kredensial yang valid maka Sender pertama kali akan mengirimkan perintah CREDS kemudian sistem receiver akan melakukan decrypt/decode/konversi pada file users, lalu menampilkannya pada receiver. Hasilnya akan menjadi seperti berikut : 

    ./receiver

    Username: Mayuri, Password: TuTuRuuu

    Username: Onodera, Password: K0sak!

    Username: Johan, Password: L!3b3rt

    Username: Seki, Password: Yuk!n3

    Username: Ayanokouji, Password: K!yot4kA

- Setelah mengetahui list kredensial, bantulah christopher untuk membuat proses autentikasi berdasarkan list kredensial yang telah disediakan. Proses autentikasi dilakukan dengan menggunakan perintah AUTH: username password kemudian jika proses autentikasi valid dan berhasil maka akan menampilkan Authentication successful . Authentication failed jika gagal.
- Setelah berhasil membuat proses autentikasi, buatlah proses transfer file. Transfer file dilakukan dari direktori Sender dan dikirim ke direktori Receiver . Proses transfer dilakukan dengan menggunakan perintah TRANSFER filename . Struktur direktorinya akan menjadi seperti berikut:


    └── soal3
       ├── Receiver
       ├── Sender
       ├── receiver.c
       └── sender.c





- Karena takut memorinya penuh, Christopher memberi status size(kb) pada setiap pengiriman file yang berhasil.

- Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

**Catatan**:

- Dilarang untuk overwrite file users secara permanen
Ketika proses autentikasi gagal program receiver akan 

- mengirim status Authentication failed dan program langsung keluar

- Sebelum melakukan transfer file sender harus login (melalui proses auth) terlebih dahulu

- Buat transfer file agar tidak duplicate dengan file yang memang sudah ada atau sudah pernah dikirim.

## Solution Soal 3
https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-3-2023-mh-it15/-/tree/main/soal_3?ref_type=heads

### Solution Soal 3A
Pertama tama buat 2 file program yaitu **sender.c dan receiver.c** yang gunanya untuk sender.c dapat mengirim pesan / perintah dan receiver.c yang akan mengeksekusinya **dengan menerapkan konsep message queue**

**pada sender.c**
```
struct message {
    long mtype;
    char mtext[256];
};
```
pada kode ini fungsinya untuk bertukar pesan antara sender dan receiver. yaitu dengan menggunakan: **mtype** dan **mtext**.
```
    key_t key = ftok("receiver.c", 65);
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    struct message msg;
    msg.mtype = 1;

```
dalam kode ini fungsinya untuk menciptakan sebuah message queue menggunakan **msgget()** dengan kunci yang didapatkan dari file "receiver.c". Jika gagal maka kode tidak akan berjalan dan Program membuat sebuah pesan dengan tipe 1 yang sama pada program receiver.c untuk digunakan nanti.

**pada receiver.c**
```
struct message {
    long mtype;
    char mtext[256];
};
```
pada kode ini fungsinya untuk bertukar pesan antara sender dan receiver (sama seperti yang ada pada sender.c). yaitu dengan menggunakan: **mtype** dan **mtext**.
```
    key_t key = ftok("receiver.c", 65);
    int msgid = msgget(key, 0666 | IPC_CREAT);

    if (msgid == -1) {
        perror("msgget");
        exit(1);
    }

    struct message msg;

    if (msgrcv(msgid, &msg, sizeof(msg.mtext), 1, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }
```
Di dalam main(), program "Receiver.c"  menciptakan sebuah message queue dengan menggunakan **ftok()** untuk menghasilkan sebuah "key" yang akan digunakan untuk berkomunikasi dengan sender. yang dimana program  menggunakan **msgrcv()** untuk menerima pesan dengan tipe 1 dari message queue. Pesan ini berisi perintah atau data yang dikirim oleh sender.

### Solution Soal 3B

**Pada Sender.c **
```
    printf("Masukkan (CREDS/AUTH: username password/TRANSFER filename/DOWNLOAD): ");
    fgets(msg.mtext, sizeof(msg.mtext), stdin);

    if (strchr(msg.mtext, '\n') != NULL) {
        msg.mtext[strcspn(msg.mtext, "\n")] = '\0';
    }
```
Program mencetak perintah ke layar sesaui inputan pemakai kode misal **CREDS/AUTH: username password/TRANSFER filename/DOWNLOAD** lalu program menggunakan fungsi **fgets()** untuk mengambil masukan dari pengguna dan menyimpannya dalam variabel msg.mtext sehingga dapat di baca oleh **receiver.c**. setelah itu program akan menghapus **karakter newline semisal ada**.

**Pada receiver.c**
```
void decodebase64(const char* input, char* output) {
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    int decodedlen = BIO_read(bio, output, strlen(input));
    output[decodedlen] = '\0'; 
    BIO_free_all(bio);
}

    FILE* file = fopen("users/users.txt", "r");
    if (file == NULL) {
        perror("fopen");
        exit(1);
    }

    if (strcmp(msg.mtext, "CREDS") == 0) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char username[256];
            char password[256];
            sscanf(line, "%[^:]:%s", username, password);

            char decodedpasword[256];
            decodebase64(password, decodedpasword);

            printf("Username: %s, Password: %s\n", username, decodedpasword);
        }
```
semisal program menerima pesan dari sender berupa perintah **CREDS** maka program akan melakukan decode user.txt menggunakan base64

```
void decodebase64(const char* input, char* output) {
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    int decodedlen = BIO_read(bio, output, strlen(input));
    output[decodedlen] = '\0'; 
    BIO_free_all(bio);
}
```
pada program ini digunakan openssl untuk melakukan **decodebase64** dengan mendekode string dalam format base64 menjadi string biasa.
```
    FILE* file = fopen("users/users.txt", "r");
    if (file == NULL) {
        perror("fopen");
        exit(1);
    }

    if (strcmp(msg.mtext, "CREDS") == 0) {
        char line[256];
        while (fgets(line, sizeof(line), file)) {
            char username[256];
            char password[256];
            sscanf(line, "%[^:]:%s", username, password);

            char decodedpasword[256];
            decodebase64(password, decodedpasword);

            printf("Username: %s, Password: %s\n", username, decodedpasword);
```
Selanjutnya pertama-tama program akan membaca file user.txt pada folder user. Selanjutnya program akan melakukan decode file user.txt tadi menggunakan openssl semisal menerima perintah **CREDS** dari sender.c dengan menggunakan ```if (strcmp(msg.mtext, "CREDS") == 0)``` lalu program akan mencetak hasilnya di layar dengn menggunakan **printf**

### Solution Soal 3C
```
    } else if (strstr(msg.mtext, "AUTH:") != NULL) {
        char username[256];
        char password[256];
        sscanf(msg.mtext, "AUTH: %s %s", username, password);

        char line[256];
        int authenticated = 0;
        while (fgets(line, sizeof(line), file)) {
            char storedusername[256];
            char storedpasword[256];
            sscanf(line, "%[^:]:%s", storedusername, storedpasword);

            char decodedpasword[256];
            decodebase64(storedpasword, decodedpasword);

            if (strcmp(username, storedusername) == 0 && strcmp(password, decodedpasword) == 0) {
                authenticated = 1;
                break;
            }
        }

        if (authenticated) {
            printf("Authentication successful.\n");
        } else {
            printf("Authentication failed.\n");
            exit(1); 
        }
```

semisal program sender.c mengirim perintah **AUTH: username password** makan program receiver.c akan menerimanya dan mengeksekusi perintah tersebut. Dengan menggunakan
```
            char decodedpasword[256];
            decodebase64(storedpasword, decodedpasword);

            if (strcmp(username, storedusername) == 0 && strcmp(password, decodedpasword) == 0) {
                authenticated = 1;
                break;
            }
        }
```
kode diatas maka perintah **AUTH** akan membaca username dan password yang sudah di decode menggunakan **base64** yang dilakukan oleh perintah **CREDS** sebelumnya. fungsi ```decodebase64(storedpassword, decodedpassword);:``` digunakan untuk mendekode kata sandi yang diambil dari file "users.txt" yang tersimpan dalam variabel storedpassword. ```if (strcmp(username, storedusername) == 0 && strcmp(password, decodedpassword) == 0) {:``` **Program membandingkan username yang diambil dari username yang ada dalam file "users.txt" (storedusername) dan membandingkan password yang diambil dari (password) dengan password yang telah didekode dari file "users.txt" (decodedpassword)**. jika semisal username dan password sesuai dengan user.txt yang sudah di decode makan program **akan memberikan pernyataan Authentication successful jika tidak sesuai maka Authentication failed**

### Pengerjaan Soal 3
1. Pertama tama buat program sender.c dan receiver.c

2. karena menggunakan opensll maka akan digunakan compile seperti ini **gcc -o sender sender.c -lcrypto dan gcc -o receiver receiver.c -lcrypto**

3. lalu gunakan perintah **CREDS** untuk mendecode user.txt 
**isinya sendiri seperti ini**

<img src="https://i.ibb.co/Y7FJJXV/3d.png" alt="3d" border="0">

yang diberikan dan menampilan hasilnya di layar setelah ter decode menggunakan base64

<img src="https://i.ibb.co/JrFJmYb/3a.png" alt="3a" border="0">

4. dengan menggunakan **perintah AUTH: username password** maka program akan mencocokan username dan password yang sudah di decode sebelumnya jika berhasil maka

<img src="https://i.ibb.co/Q9dD09Y/3b.png" alt="3b" border="0">

jika gagal (disitu saya menambahkan koma)

<img src="https://i.ibb.co/82X1mPf/3c.png" alt="3c" border="0">

### Problem Soal 3
Fungsi transfer tidak berfungsi
```

sender.c
    if (strstr(msg.mtext, "TRANSFER") != NULL) {
        char filename[256];
        sscanf(msg.mtext, "TRANSFER %s", filename);

        char userfile[512]; 
        snprintf(userfile, sizeof(userfile), "Sender/%s", filename);

        FILE* filesend = fopen(userfile, "r");
        if (filesend == NULL) {
            perror("fopen");
            exit(1);
        }

        struct message filemass;
        filemass.mtype = 2;  
        fread(filemass.mtext, 1, sizeof(filemass.mtext), filesend);

        if (msgsnd(msgid, &filemass, sizeof(filemass.mtext), 0) == -1) {
            perror("msgsnd");
            exit(1);
        }

        printf("File '%s' has sent.\n", filename);

        fclose(filesend);

Receiver.c
else if (strstr(msg.mtext, "TRANSFER") != NULL) {
        char filename[256];
        sscanf(msg.mtext, "TRANSFER %s", filename);

        if (access("Receiver", F_OK) == -1) {
            mkdir("Receiver", 0777);
        }

        char receiverfile[512]; 
        snprintf(receiverfile, sizeof(receiverfile), "Receiver/%s", basename(filename));

        FILE* fileexist = fopen(receiverfile, "r");
        if (fileexist) {
            printf("File '%s' already exists.\n", filename);
            fclose(fileexist);
        } else {
            FILE* receivedFile = fopen(receiverfile, "w");
            fwrite(msg.mtext, 1, strlen(msg.mtext), receivedFile);
            fclose(receivedFile);
            printf("File '%s' transferred to Receiver.\n", filename);
        }
    }
```
fungis sender hanya mencetak massage file berhasil terkirim di terminal tetapi file belum masuk dari folder sender ke folder receiver

pada terminal terdapat massage bahwa file.txt sudah terkirim

<img src="https://i.ibb.co/jL3M8K0/3e.png" alt="3e" border="0">

Tetapi file.txt tersebut tidak mau masuk ke folder receiver

<img src="https://i.ibb.co/FXDMvF4/3f.png" alt="3f" border="0"> (folder receiver masih kosong)

# SOAL 4

Takumi adalah seorang pekerja magang di perusahaan Evil Corp. Dia mendapatkan tugas untuk membuat sebuah public room chat menggunakan konsep socket. Ketentuan lebih lengkapnya adalah sebagai berikut:

## Problem Soal 4

A. Client dan server terhubung melalui socket. 

B. Server berfungsi sebagai penerima pesan dari client dan hanya menampilkan pesan saja.  

C. Karena resource Evil Corp sedang terbatas buatlah agar server hanya bisa membuat koneksi dengan 5 client saja. 

## Solution Soal 4

[Source Code Soal 4](https://gitlab.com/subkhanmasudi8/sisop-praktikum-modul-3-2023-mh-it15/-/tree/main/soal_4?ref_type=heads)

### Solution Soal 4 A
Pada sisi Client
```
sockfd = socket(AF_INET, SOCK_STREAM, 0);
server_addr.sin_family = AF_INET;
server_addr.sin_addr.s_addr = inet_addr(ip);
server_addr.sin_port = htons(port);

...

int err = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
if (err == -1) {
    printf("ERROR: connect\n");
    return EXIT_FAILURE;
}

...

```

Pada sisi Server
```
listenfd = socket(AF_INET, SOCK_STREAM, 0);
serv_addr.sin_family = AF_INET;
serv_addr.sin_addr.s_addr = inet_addr(ip);
serv_addr.sin_port = htons(port);

...

if (bind(listenfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    perror("ERROR: Socket binding failed");
    return EXIT_FAILURE;
}

...

while (1) {
    socklen_t clilen = sizeof(cli_addr);
    connfd = accept(listenfd, (struct sockaddr *)&cli_addr, &clilen);

    ...
}

```
Pada kedua program tersebut, **socket()** digunakan untuk membuat socket antara keduanya. Lalu, **bind()** digunakan pada program server untuk mengikat socket ke alamat dan port tertentu. Kemudian, **accept()** digunakan pada program server untuk menerima koneksi dari program client. Terakhir ada **connect()** untuk menghubungkan program client ke program server dengan alamat yang sudah ditentukan.

### Solution Soal 4 B

Pada sisi Server
```
void *handle_client(void *arg) {
    char buff_out[BUFFER_SZ];
    char name[10];
    int leave_flag = 0;

    cli_count++;
    client_t *cli = (client_t *)arg;

    if (recv(cli->sockfd, name, 10, 0) <= 0 || strlen(name) < 2 || strlen(name) >= 10 - 1) {
        printf("Nama invalid.\n");
        leave_flag = 1;
    } else {
        strcpy(cli->name, name);
        sprintf(buff_out, "%s telah bergabung\n", cli->name);
        printf("%s", buff_out);
    }

    bzero(buff_out, BUFFER_SZ);

    while (1) {
        if (leave_flag) {
            break;
        }

        int receive = recv(cli->sockfd, buff_out, BUFFER_SZ, 0);
        if (receive > 0) {
            if (strlen(buff_out) > 0) {
                str_trim_lf(buff_out, strlen(buff_out));
                printf("%s -> %s\n", buff_out, cli->name);
            }
        } else if (receive == 0 || strcmp(buff_out, "exit") == 0) {
            sprintf(buff_out, "%s telah meninggalkan room chat\n", cli->name);
            printf("%s", buff_out);
            leave_flag = 1;
        } else {
            printf("ERROR: -1\n");
            leave_flag = 1;
        }

        bzero(buff_out, BUFFER_SZ);
    }

    close(cli->sockfd);
    queue_remove(cli->uid);
    free(cli);
    cli_count--;
    pthread_detach(pthread_self());

    return NULL;
}

```
Pada fungsi **handle_client()** di atas, server dapat menerima pesan dari client menggunakan **recv()**. Kemudian, jika server menerima dan mengecek inputan dari client dengan panjang string lebih dari 0, maka server akan menampilkan inputan pesan yang dikirim dari client sekaligus menampilkan nama client yang mengirim pesan tersebut. Jika server mengcompare dan menemukan inputan pesan yang dikirim dari client bernilai "exit", maka server akan menampilkan pesan bahwa client dengan nama tertentu telah meninggalkan room chat. Setelah itu, server akan menutup socket client yang bersangkutan dan meremove client tersebut dari antrian serta mengurangi jumlah client yang sedang terhubung dan memutus segala sumber daya yang sebelumnya dialokasikan untuk client tersebut.

### Solution Soal 4 C
```
#define MAX_CLIENTS 5

...

int main(int argc, char **argv) {
    while (1) {
        socklen_t clilen = sizeof(cli_addr);
        connfd = accept(listenfd, (struct sockaddr *)&cli_addr, &clilen);

        if (cli_count == MAX_CLIENTS) {
            printf("Client telah penuh. Rejected: ");
            print_client_addr(cli_addr);
            printf(":%d\n");
            close(connfd);
            continue;
        }

...
```
Pertama, kami mendefine bahwa maksimal client yang dapat terhubung ke server sebanyak 5 clients sesuai soal. Kemudian, program akan melakukan pengecekan apakah jumlah client yang terhubung sama dengan jumlah maksimal client yang dapat terhubung. Jika ya, maka pada server akan menampilkan pesan bahwa client telah penuh dan menampilkan alamat IP dari client yang tertolak tersebut. Program pun juga akan menutup socket yang digunakan sebagai koneksi dengan client yang tertolak untuk mencegah client yang tertolak terus terhubung. Terakhir, program tetap melanjukan proses pengecekan terhadap jumlah maksimal client yang dapat terhubung selama program server masih dijalankan.

## Pengerjaan Soal 4
1. Pertama, buat 2 folder yang berbeda dengan isi masing-masing folder adalah file program client dan server.

2. Jalankan program client dan server

<a href="https://ibb.co/hL8Y2kp"><img src="https://i.ibb.co/hL8Y2kp/1.png"></a>

3. Masukkan nama dan pesan pada terminal program client

<a href="https://ibb.co/NFDNmff"><img src="https://i.ibb.co/NFDNmff/2.png"></a>

4. Cobalah membuka terminal lain untuk menghubungkan client yang lain ke server dan lakukan kembali seperti langkah 2 dan 3

<a href="https://ibb.co/hXmys5J"><img src="https://i.ibb.co/hXmys5J/3.png" alt="3" border="0"></a>

5. Coba ketikkan **exit** pada masing-masing terminal client yang terhubung ke server, maka server akan memunculkan pesan bahwa client telah meninggalkan chatroom

<a href="https://ibb.co/mq4BYv9"><img src="https://i.ibb.co/mq4BYv9/4.png" alt="4" border="0"></a>

6. Setelah client ke-6 melakukan percobaan untuk terhubung dengan server, maka server akan memunculkan pesan bahwa client telah penuh dan mereject alamat IP dari client ke-6 tersebut

<a href="https://ibb.co/pPGKcYW"><img src="https://i.ibb.co/pPGKcYW/5.jpg" alt="5" border="0"></a>

```
Catatan : Mohon untuk mengklik screenshot foto yang terlampir pada section pengerjaan soal 4 untuk melihat foto yang lebih jelas
```
