#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>


int main(int argc, char *argv[]){

    //banyak yang perlu dideklarasi
    char namafile[] = "lirik.txt";
    char namafile_output[] = "thebeatles.txt";
    char logfile[] = "frekuensi.log";
    char input[1000];
    char chek;
    int count = 0, len;

    //untuk input -kata/-huruf
    int is_kata = strcmp(argv[1], "-kata") == 0;
    int is_huruf = strcmp(argv[1], "-huruf") == 0;

    //waktu buat log
    time_t sekarang;
    time(&sekarang);
    struct tm *local = localtime(&sekarang);
    char timedata[20];
    strftime(timedata, 20, "%d/%m/%y %H:%M:%S", local);

    int fd1[2], fd2[2]; //2 pipa

    //bagian input program
    if(is_huruf){
        printf("masukkan huruf: ");
    }
    else if(is_kata){
        printf("masukkan kata: ");
    }
    scanf("%s", input);

    pid_t pid;

    //memastikan pipa bekerja
    if (pipe(fd1) == -1 || pipe(fd2) == -1) {
        fprintf(stderr, "Pipe failed");
        return 1;
    }

    pid = fork();

    if (pid < 0) {
        fprintf(stderr, "Fork failed");
        return 1;
    }
    /*                                        parent process                                    */
    if (pid > 0) {
        //------------------------------------------------------------------------------------- [1]
        //awali dengan ini -1-
        char con[1000];
        close(fd1[0]); //1

        //ini ada yang perlu diwrite
        FILE *file = fopen(namafile, "r");                                  
        FILE *file_output = fopen(namafile_output, "w");                     

        char hurufd;                                                            //sebagai pointer 
        while((hurufd = fgetc(file)) != EOF){                                   //jika file lirik belum EOF periksa terus
            if(isalpha(hurufd) || isspace(hurufd)){                             //jika dia huruf dan spasi
                fputc(hurufd, file_output);                                     //maka isinya bakal nambah
            }
        }
        fclose(file);                                                           //close file
        fclose(file_output);                                                    //close file_output
        write(fd1[1], input, strlen(input)+1);                                  //mengirim input ke child
        close(fd1[1]); // 2

        printf("done ubah liriknya\n");
        //------------------------------------------------------------------------------------- [\1]

        wait(NULL);                                                             //nunggu childe proses

        //------------------------------------------------------------------------------------- [3]
        close(fd2[1]); // 5
        // Hasil akhir

        read(fd2[0], con, 1000);
        FILE *filelog = fopen(logfile, "a");
        if(is_kata){
            fprintf(filelog, "[%s] [KATA] Kata '%s' muncul sebanyak %s kali dalam file '%s'.\n", timedata, input, con, namafile_output);
        } 
        if(is_huruf){
            fprintf(filelog, "[%s] [HURUF] Huruf '%s' muncul sebanyak %s kali dalam file '%s'.\n", timedata, input, con, namafile_output);
        }

        fclose(filelog);

        printf("done frekuensi\n");

        
        close(fd2[0]); // 6
        //------------------------------------------------------------------------------------- [\3]
        printf("done all\n\n");
    }
     /*                                        child process                                      */
    else{
    
        close(fd1[1]); // 2
        //------------------------------------------------------------------------------------- [2]
        //nyambungin dari parent ke child
        char con[1000];
        read(fd1[0], con, 1000);
        printf("done buffer\n");

        close(fd1[0]); // 3
        close(fd2[0]); // 4

        FILE *output_file = fopen(namafile_output, "r");             
        if(is_kata){
            len = strlen(input);

            while((chek = fgetc(output_file)) != EOF){                   //memeriksa input dengan isi file thebeatles.txt
                if(chek == input[0]){
                    int i;
                    for(i = 1; i < len; i++){                           
                        chek = fgetc(output_file);
                        if(chek != input[i]){
                            break;
                        }    
                    }
                    if(i == len){                                        
                        count ++;
                    }
                }
            }
        }
        else if(is_huruf){
            len = strlen(input);

            while((chek = fgetc(output_file)) != EOF){
                if(chek == input[0]){
                    int i;
                    for(i = 1; i < len; i++){
                        chek = fgetc(output_file);
                        if(chek != input[i]){
                            break;
                        }    
                    }
                    if(i == len){
                        count ++;
                    }
                }
            }
        }
        fclose(output_file);
        printf("done ngitung\n");
        if(is_kata){
        printf("kata '%s' muncul sebanyak %d kali dalam file '%s'.\n", input, count, namafile_output);
        }
        if(is_huruf){
        printf("huruf '%s' muncul sebanyak %d kali dalam file '%s'.\n", input, count, namafile_output);
        }
        snprintf(con, sizeof(con), "%d", count);                              
        write(fd2[1], con, strlen(con) + 1); 
        close(fd2[1]); 
        }
    

    return 0;
}
